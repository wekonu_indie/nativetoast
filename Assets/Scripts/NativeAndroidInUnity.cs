﻿using UnityEngine;
using UnityEngine.UI;

public class NativeAndroidInUnity : MonoBehaviour
{
    [SerializeField] Button btn_ShowToastButton;
    [SerializeField] string toastText = "This is a Toast";

    void Start()
    {
        btn_ShowToastButton.onClick.AddListener(OnShowToastClicked);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void OnShowToastClicked()
    {
#if UNITY_ANDROID
        ShowAndroidToast();
#else
        if (Debug.isDebugBuild)
        {
            Debug.Log("No Toast setup for this platform");
        }
#endif
    }

    void ShowAndroidToast()
    {
#if !UNITY_ANDROID
        if(Debug.isDebugBuild)
        {
            Debug.Log("No Toast setup for this platform");
        }
        return;
#endif

        AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");

        //create a class reference of unity player activity
        AndroidJavaClass unityActivity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        //create an object array of 3 size
        object[] toastParams = new object[3];

        //set the first object in the array as current activity reference
        toastParams[0] = unityActivity.GetStatic<AndroidJavaObject>("currentActivity");

        //set the second object in the array as the CharSequence to be displayed
        toastParams[1] = "This is a Toast";

        //set the third object in the array as the duration of the toast from
        toastParams[2] = toastClass.GetStatic<int>("LENGTH_LONG"); // or LENGTH_SHORT

        AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", toastParams);

        toastObject.Call("show");
    }
}
